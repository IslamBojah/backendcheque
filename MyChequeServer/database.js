var mysql=require('mysql')

exports.connect =function(connectionData ,callback){
    var con = mysql.createConnection({
        host: connectionData.host,
        user: connectionData.user,
        password: connectionData.password,
        database: connectionData.database
        }); 
        con.connect(function(err) {
        if (err) callback(err);
        callback(null, con);
        });
        
};