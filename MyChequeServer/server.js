var express = require('express')
var app = express();
const jwt = require('jsonwebtoken')
const path = require('path')
    // image
const multer = require('multer');
const fs = require('fs')

var port = 9091;


var bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.json())
require('dotenv').config()

var message = require('./message')
const databaseInfo = {
    host: process.env.host,
    user: process.env.user,
    password: process.env.password,
    database: process.env.database,
}


// storage for image 
const storage = multer.diskStorage({
    destination(req, file, callback) {
        callback(null, './images')
    },
    filename(req, file, callback) {
        callback(null, `${file.fieldname}_${Date.now()}_${file.originalname}`)
    }
})

//upload image
const upload = multer({ storage: storage })


//var upload = multer({ dest: './images' });


app.post('/api/auth', function(req, res) {
    console.log(req.body);
    error = {}
    var data = {
        user_phone: req.body.user_phone,
        user_password: req.body.user_password,
    };

    if (Object.keys(error).length == 0) {
        message.SignIn(databaseInfo, data, function(err, result) {
            if (err) {
                res.status(400);
                res.end("error" + err);
            } else {
                if (result.length == 1) {
                    res.status(201);
                    res.json({ token: result[0].token, user_id: result[0].id, success: true, user_name: result[0].user_name })
                } else {
                    res.status(201);
                    res.json({ success: false, error: 'Invalid mobile number or password' })
                }
            }
        });
    } else {
        res.json({ success: false, error: 'Invalid userphone or password' })
    }
});

function toAuthtication(username) {
    return jwt.sign({
        username: username,
    }, process.env.securetkey)
}

app.post('/api/signUp', function(req, res) {
    error = {}
    var data = {
        user_name: req.body.user_name,
        user_phone: req.body.user_phone,
        user_password: req.body.user_password,
        token: toAuthtication(req.body.user_name)
    };


    if (Object.keys(error).length == 0) {
        message.signUp(databaseInfo, data, function(err, result) {
            if (err) {
                res.status(400);
                res.end("error:" + err);
            } else {
                res.status(201);
                if (result == false) {
                    res.json({ success: false, error: 'this user is signed up' })
                } else {
                    message.SignIn(databaseInfo, data, function(err, result) {
                        res.json({ success: true, token: result[0].token, user_id: result[0].id, user_name: result[0].user_name })
                    });
                }
            }
        })

    } else {
        res.json({ success: false, error: 'Invaild userphone or password' })
    }
})

app.post('/api/banks', function(req, res) {
    data = {
        lan: req.body.lan
    }
    message.getAllbanks(databaseInfo, data, function(err, result) {
        if (err) {
            res.status(400);
            res.end("error" + err);
        } else {
            res.status(201);
            res.json(result)
        }
    })
})

/*app.post('/api/banks',function(req,res){
    data={
        bank_name:req.body.bank_name
    }
    message.InsertBank(databaseInfo,data,function(err,result){
        if(err){
            res.status(400);
            res.end("error"+err);
        }
        else{
            res.status(201);
            res.end('success')
        }  
    })
});
*/
app.get('/api/coins', function(req, res) {
    message.getAllCoins(databaseInfo, function(err, result) {
        if (err) {
            res.status(400);
            res.end("error" + err);
        } else {
            res.status(201);
            res.json(result)
        }
    })
})

app.post('/api/book', function(req, res) {
    data = {
        book_name: req.body.book_name,
        book_size: req.body.book_size,
        bank_name: req.body.bank_name,
        coin_name: req.body.coin_name,
        status: req.body.status,
        user_id: req.body.user_id,
        date: req.body.date,
        available_size: req.body.book_size
    }
    message.InsertBook(databaseInfo, data, function(err, result) {
        if (err) {
            res.status(400);
            res.end(err)
        } else {
            res.status(201)
            res.json(result)
        }
    })
});

app.post('/api/DeleteBook', function(req, res) {
    data = {
        book_id: req.body.book_id
    }
    message.DeleteBook(databaseInfo, data, function(err, result) {
        if (err) {
            res.status(400);
            res.end(err)
        } else {
            res.status(201)
            res.end('success')
        }
    })
});

app.put('/api/book', function(req, res) {
    data = {
        book_id: req.body.book_id,
        book_name: req.body.book_name,
        coin_name: req.body.coin_name,
        bank_name: req.body.bank_name,
        book_size: req.body.book_size,
        available_size: req.body.book_size
    }

    message.updateBook(databaseInfo, data, function(err, result) {
        if (err) {
            res.status(400);
            res.end('err' + err);
        } else {
            res.status(201);
            res.end('success')
        }
    })
})

app.put('/api/bookAvailableSize', function(req, res) {
    // console.log(req.body)
    data = {
        book_id: req.body.book_id,
        available_size: req.body.available_size
    }

    message.UpdateBookAvaliable(databaseInfo, data, function(err, result) {
        if (err) {
            res.status(400)
            res.end('err' + err)
        } else {
            res.status(201)
            res.end('success')
        }
    })

})
app.post('/api/AllUserBook', function(req, res) {
    data = {
        user_id: req.body.user_id
    }
    message.getUserBooks(databaseInfo, data, function(err, result) {
        if (err) {
            res.status(400);
            res.end('err' + err)
        } else {
            res.status(201)
            res.json(result)
        }
    })
})

app.post('/api/cheque', upload.array('photo', 3), function(req, res) {
    console.log(req.body);
    console.log('file', req.files)
    console.log('body', req.body)
    data = {
        book_id: req.body.book_id,
        date: req.body.date,
        value: req.body.value,
        for_whom: req.body.for_whom,
        cheque_no: req.body.cheque_no,
        is_primary: req.body.is_primary,
        is_crossed: req.body.is_crossed,
        Note: req.body.Note
    }
    message.InsertNewCheque(databaseInfo, data, function(err, result) {
        if (err) {
            res.status(400)
            res.end(err)
        } else {
            res.status(201)
            res.json(result)
        }
    })
})

app.post('/api/AllBookcheques', function(req, res) {
    data = {
        book_id: req.body.book_id
    }
    message.getBookCheques(databaseInfo, data, function(err, result) {
        if (err) {
            res.status(400);
            res.end(err)
        } else {
            res.status(201);
            res.json(result)
        }
    })
});

app.delete('/api/cheque', function(req, res) {
    console.log(req.query.cheque_id)
    data = {
        cheque_id: req.query.cheque_id
    }
    message.deleteCheque(databaseInfo, data, function(err, result) {
        if (err) {
            res.status(400);
            res.end(err)
        } else {
            res.status(201)
            res.end('success');
        }
    })
})

app.put('/api/cheque', function(req, res) {
    data = {
        cheque_id: req.body.cheque_id,
        date: req.body.date,
        value: req.body.value,
        for_whom: req.body.for_whom,
        cheque_no: req.body.cheque_no,
        is_primary: req.body.is_primary,
        is_crossed: req.body.is_crossed,
        Note: req.body.Note,
    }
    message.updateChequeStatus(databaseInfo, data, function(err, result) {
        if (err) {
            res.status(400)
            res.end(err)
        } else {
            res.status(201)
            res.end('success')
        }
    })


})
app.put('/api/chequeupdate', function(req, res) {
    data = {
        cheque_id: req.body.id,
        status: req.body.status
    }
    message.updateCheque(databaseInfo, data, function(err, result) {
        if (err) {
            res.status(400);
            res.end(err)
        } else {
            res.status(201)
            res.end('success');
        }
    })
})


app.post('/api/recivedCheque', upload.single('avatar'), function(req, res) {
    //  console.log(req)
    //   console.log(req.body)
    console.log('file', req.file)
        //console.log('file', req.files)
        //console.log('body', req.body)

    fs.readFile(req.file.path, (err, contents) => {
        if (err) {
            console.log('Error: ', err);
        } else {
            console.log('File contents ', contents);
        }
    });

    data = {
        user_id: req.body.user_id,
        bank_name: req.body.bank_name,
        date: req.body.date,
        value: req.body.value,
        coin: req.body.coin,
        from_who: req.body.from_who,
        cheque_no: req.body.cheque_no,
        cheque_status: req.body.cheque_status,
        is_crossed: req.body.is_crossed,
        Note: req.body.Note
    }
    message.InsertRecivedcheque(databaseInfo, data, function(err, result) {
        if (err) {
            res.status(400);
            res.end('err' + err)
        } else {
            res.status(201);
            res.json(result)
        }
    })
})

app.put('/api/recivedCheque', function(req, res) {
    data = {
        id: req.body.id,
        bank_name: req.body.bank_name,
        date: req.body.date,
        value: req.body.value,
        coin: req.body.coin,
        from_who: req.body.from_who,
        cheque_no: req.body.cheque_no,
        cheque_status: req.body.cheque_status,
        is_crossed: req.body.is_crossed,
        Note: req.body.Note
    }
    message.UpdateRecivedCheque(databaseInfo, data, function(err, result) {
        if (err) {
            res.status(400)
            res.end('err' + err)
        } else {
            res.status(201);
            res.end('success')
        }
    })
})



app.delete('/api/recivedCheque', function(req, res) {
    data = {
        id: req.query.id
    }
    message.deleteRecievedCheque(databaseInfo, data, function(err, result) {
        if (err) {
            res.status(400);
            res.end('err' + err)
        } else {
            res.status(201)
            res.end('success')
        }
    })
})

app.post('/api/recivedChequesUser', function(req, res) {

    data = {
        user_id: req.body.user_id
    }
    message.getAllRecievedCheques(databaseInfo, data, function(err, result) {
        if (err) {
            res.status(400)
            res.end('err' + err)
        } else {
            res.status(201)
            res.json(result)
        }
    })
})

app.put('/api/recivedChequeStatus', function(req, res) {
    // console.log(req.body)
    data = {
        id: req.body.id,
        status: req.body.status
    }
    message.UpdateRecivedChequeStatus(databaseInfo, data, function(err, result) {
        if (err) {
            res.status(400)
            res.end('err' + err)
        } else {
            res.status(201);
            res.end('success')
        }
    })

})

app.get('/api/alluserCheques', function(req, res) {
    data = {
        user_id: req.query.user_id
    }
    message.getAllCheques(databaseInfo, data, function(err, result) {
        if (err) {
            res.status(400)
            res.end('err' + err)
        } else {
            //  console.log(result)
            res.status(201)
            res.json(result)

        }
    })
})

app.put('/api/changePassword', function(req, res) {
    data = {
        user_id: req.body.user_id,
        user_password: req.body.user_password,
        new_password: req.body.new_password
    }
    message.confirmUserPassword(databaseInfo, data, function(err, result) {
        if (result.length == 1 && data.user_password != data.new_password) {
            message.UpdatePassword(databaseInfo, data, function(error, resultt) {
                if (err) {
                    res.status(400)
                    res.end(error + 'err')
                } else {
                    res.status(200)
                    res.json({ success: true })
                }
            })
        } else {
            res.status(201)
            res.json({ success: false })

        }
    })
})

app.put('/api/changePhone', function(req, res) {
    data = {
        user_id: req.body.user_id,
        old_phone: req.body.old_phone,
        new_phone: req.body.new_phone
    }
    message.confirmUserPhone(databaseInfo, data, function(err, result) {
        if (result.length == 1 && data.old_phone != data.new_phone) {
            message.UpdatePhone(databaseInfo, data, function(error, resultt) {
                if (err) {
                    res.status(400)
                    res.end(error + 'err')
                } else {
                    res.status(200)
                    res.json({ success: true })
                }
            })
        } else {
            res.status(201)
            res.json({ success: false })

        }
    })
})

app.listen(port);