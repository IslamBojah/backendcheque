
var db =require ('./database')

exports.SignIn=function(connnectionData, req, callback){

    db.connect(connnectionData ,function(err,data){
        if(err){
            callback(err);
            return;
        }
        data.query(`SELECT * FROM users where user_phone=('${req.user_phone}') and user_password=('${req.user_password}')`,function(err,result){
            callback(err,result)
        });
    });
};

exports.signUp=function(connnectionData, req, callback){
    db.connect(connnectionData ,function(err,data){
        if(err){
            callback(err);
            return;
        }
        data.query(`SELECT * FROM users where user_phone=('${req.user_phone}')`,function(err,result){
            if(result.length ==0){
                db.connect(connnectionData,function(err,d){
                    if(err){
                        callback(err);                        
                    }
                    data.query(`INSERT INTO users SET?`,req,function(err,res){
                        callback(err,'success')
    
                    });

                });
             
            }
            else{
                callback(err,false)
            }
        });
    });
};

exports.getAllbanks=function(connnectionData,req,callback){
    db.connect(connnectionData,function(err,data){
        if(err){
            callback(err)
            return ;
        }
        else{
            data.query(`SELECT * from banks where lan=('${req.lan}')` , function(err,result){
                callback(err,result)
            })
        }
    })
}

exports.InsertBank=function(connnectionData,req,callback){
    db.connect(connnectionData,function(err,data){
        if(err){
            callback(err)
            return;
        }
        else{
            data.query(`INSERT INTO banks set?`,req,function(err,result){
                callback(err,result)
            })
        }
    })
}

exports.getAllCoins=function(connnectionData,callback){
    db.connect(connnectionData,function(err,data){
        if(err){
            callback(err)
            return;
        }
        else{
            data.query(`SELECT * from coins` , function(err,result){
                callback(err,result)
            })
        }
    })
}

exports.InsertBook=function(connnectionData,req,callback){
    db.connect(connnectionData,function(err,data){
        if(err){
            callback(err)
            return;
        }
        else{
            data.query(`INSERT INTO books set?`,req , function(err,result){

                db.connect(connnectionData,function(err,data){
                   
                    data.query(`SELECT * FROM books WHERE book_id=(select max(book_id) from books)`,function(err,res){
                        callback(err,res)
                    })
                })
            })
        }  
    })
}

exports.DeleteBook=function(connnectionData,req,callback){
    db.connect(connnectionData,function(err,data){
        if(err){
            callback(err)
            return;
        }
        else{
            data.query(`DELETE FROM books where book_id=('${req.book_id}')`,function(err,result){
                callback(err,result)
            })
        }
    })
}

exports.UpdateBookAvaliable=function(connectionData,req,callback){
    db.connect(connectionData,function(err,data){
        if(err){
            callback(err)
            return
        }
        else{
            data.query(`UPDATE books SET available_size=('${req.available_size}') where book_id=('${req.book_id}')`,function(err,result){
                
                callback(err,result)
            })
        }
    })
}

exports.updateBook=function(connectionData,req,callback){
    db.connect(connectionData,function(err,data){
        if(err){
            callback(err);
            return
        }
        else{
            data.query(`UPDATE books SET book_name=('${req.book_name}') , coin_name=('${req.coin_name}') ,
            bank_name=('${req.bank_name}') , book_size=('${req.book_size}') , available_size=('${req.available_size}')
             where book_id=('${req.book_id}') `,function(err,result){
                callback(err,result);
            })
        }
    })
}

exports.getUserBooks=function(connectionData,req,callback){
    db.connect(connectionData,function(err,data){
        if(err){
            callback(err);
            return
        }
        else{
            data.query(`SELECT * FROM books where user_id=('${req.user_id}')`,function(err,result){
                
                callback(err,result);
            })
        }
    })
}

exports.InsertNewCheque=function(connectionData,req,callback){
    db.connect(connectionData,function(err,data){
        if(err){
            callback(err)
            return;
        }
        else{
            data.query(`INSERT INTO cheques (book_id , value , date , for_whom , cheque_no , is_primary , is_crossed , Note)
            VALUES ( '${req.book_id}' , '${req.value}','${req.date}'  , '${req.for_whom}' , '${req.cheque_no}',
            '${req.is_primary}' , '${req.is_crossed}' , '${req.Note}'
            )`,function(err,result){
                db.connect(connectionData,function(error,da){
                    da.query(`SELECT * FROM cheques WHERE cheque_id=(select max(cheque_id) from cheques)`,function(err,res){
                        callback(err,res);
                    })

                })
            
            })
        }
    })
}

exports.getBookCheques=function(connectionData,req,callback){
    db.connect(connectionData,function(err,data){
        if(err){
            callback(err)
            return;
        }
        else{
            data.query(`SELECT * FROM cheques where book_id=('${req.book_id}')`,function(err,result){
                callback(err,result)
            })
        }
    })
}

exports.getAllCheques=function(connectionData,req,callback){
    db.connect(connectionData,function(err,data){
        if(err){
            callback(err)
            return
        }
        else{
            data.query(`SELECT * FROM books RIGHT JOIN cheques on books.book_id = cheques.book_id WHERE user_id=('${req.user_id}') `,function(err,result){
                callback(err,result)
            })
        }
    })
}

exports.deleteCheque=function(connectionData,req,callback){
    db.connect(connectionData,function(err,data){
        if(err)
        {
            callback(err)
            return;
        }
        else{
            data.query(`DELETE FROM cheques where cheque_id=('${req.cheque_id}')`,function(err,result){
                callback(err,result);
            })
        }
    })
}

exports.updateChequeStatus=function(connectionData,req,callback){
    db.connect(connectionData,function(err,data){
        if(err){
            callback(err)
            return
        }
        else{
            data.query(`UPDATE cheques SET date=('${req.date}') , value=('${req.value}') , for_whom=('${req.for_whom}'),
            cheque_no=('${req.cheque_no}') , is_primary=('${req.is_primary}') , is_crossed=('${req.is_crossed}') , Note=('${req.Note}')
            where cheque_id=('${req.cheque_id}')`,function(err,result){
                callback(err,result);
            })
        }
    })
}

exports.updateCheque=function(connectionData,req,callback){
    db.connect(connectionData,function(err,data){
        if(err){
            callback(err)
            return;
        }
        else{
  
            data.query(`UPDATE cheques SET status=('${req.status}') where cheque_id=('${req.cheque_id}')` ,function(err,result){
                callback(err,result);
            })
        }
    })
}

exports.InsertRecivedcheque=function(connectionData,req,callback){
    db.connect(connectionData,function(err,data){
        if(err){
            callback(err);
            return;
        }
        else{
            data.query(`INSERT INTO received_cheques SET?`,req,function(err,result){
                db.connect(connectionData,function(err,da){
                    da.query(`SELECT * FROM received_cheques WHERE id=(select max(id) from received_cheques)`,function(err,res){
                        callback(err,res)
                    })

                })
            })
        }
    })
}

exports.UpdateRecivedCheque=function(connectionData,req,callback){
    db.connect(connectionData,function(err,data){
        if(err){
            callback(err);
            return
        }
        else{
            data.query(`UPDATE received_cheques SET 
                bank_name=('${req.bank_name}'), date=('${req.date}'),
                value=('${req.value}'), from_who=('${req.from_who}'),cheque_no=('${req.cheque_no}'),
                cheque_status=('${req.cheque_status}'),coin=('${req.coin}') , is_crossed=('${req.is_crossed}')
                ,Note=('${req.Note}')
                where id=('${req.id}')`,function(err,result){
                    callback(err,result)
                })
        }
    })
}


exports.deleteRecievedCheque=function(connectionData,req,callback){
    db.connect(connectionData,function(err,data){
        if(err){
            callback(err)
            return;
        }
        else{
            data.query(`DELETE FROM received_cheques WHERE id=('${req.id}')`,function(err,result){
                callback(err,result)
            })
        } 
    })
}

exports.getAllRecievedCheques=function(connectionData,req,callback){
    db.connect(connectionData,function(err,data){
        if(err){
            callback(err)
            return
        }
        else{
            data.query(`SELECT * from received_cheques where user_id=('${req.user_id}') and status!='2'`,function(err,result){
                callback(err,result)
            })
        }
    })
}

exports.UpdateRecivedChequeStatus=function(connectionData,req,callback){
    db.connect(connectionData,function(err,data){
        if(err){
            callback(err)
            return
        }
        else{
            data.query(`UPDATE received_cheques SET status=('${req.status}') where id=('${req.id}')`,function(err,result){
                callback(err,result)
            })
        }
    })
}


exports.confirmUserPassword=function(connectionData,req,callback){
    db.connect(connectionData,function(err,data){
        if(err){
            callback(err)
            return
        }
        else{
            data.query(`SELECT * from users where id=('${req.user_id}') and user_password=('${req.user_password}')`,function(err,result){
                callback(err,result)
            })
        }
    })
}
exports.UpdatePassword=function(connectionData,req,callback){
    db.connect(connectionData,function(err,data){
        if(err){
            callback(err)
            return
        }
        else{
            data.query(`UPDATE users SET user_password=('${req.new_password}') where id=('${req.user_id}')`,function(err,result){
                callback(err,result)
            })
        }
    })
}

exports.confirmUserPhone=function(connectionData,req,callback){
    db.connect(connectionData,function(err,data){
        if(err){
            callback(err)
            return
        }
        else{
            data.query(`SELECT * from users where id=('${req.user_id}') and user_phone=('${req.old_phone}')`,function(err,result){
                callback(err,result)
            })
        }
    })
}
exports.UpdatePhone=function(connectionData,req,callback){
    db.connect(connectionData,function(err,data){
        if(err){
            callback(err)
            return
        }
        else{
            data.query(`UPDATE users SET user_phone=('${req.new_phone}') where id=('${req.user_id}')`,function(err,result){
                callback(err,result)
            })
        }
    })
}